# Información

Prueba técnica realizada para DentOS, a continuación especifico las tecnologias usadas:

####  Tecnologias
- **Aws amazon (EC2)**: Creé un servidor EC2 (Instancia de Linux) para crear todo el ambiente de deploy (Simulado, esta corriendo script de ejecución para laravel y angular), se configuró la instancia para permitir el acceso por ssh por medio de autenticación por contraseña, ya que por defecto AWS solo permite conexiones por archivos PEM.
- **Postgres**: Como motor de base de datos se utilizó postgresql, realizé la instalación manual del mismo en su versión 12, configuración para permitir el acceso a usuarios de cualquier ip (Acceso remoto).
- **Group Security AWS**: Se modificó un grupo de seguridad de aws para permitir en las "Reglas de Entrada" y "Salida" los puertos 80, 8080, 5432 (Postgres), 22 (ssh), 4200 (angular), 8000 (laravel)
- **Laravel 8.72.0**: Utilizé la versión mas reciente de laravel y se instaló para ser usado como API, implementando desde los Endpoints, hasta las capas de seguridad de JWT (Json Web Tokens, más específicamente Bearer Token).
- **Angular Front**: Para consumir el backend (API en Laravel) se creó toda la vista en Angular usando "Angular Material, HTTPClient, Rxjs, Observables".
- **Npm 6.14.15**: Npm como administrador de paquetes para Angular.
- **php 7.4.3**: Esta es la versión utilizada para soportar Laravel.

A continuación se describe el proceso de instalación:

Todo se encuentra subido en el repositorio [link](https://gitlab.com/ingrejhon/blog-with-login.git)

Ramas a Utilizar [back_laravel](https://gitlab.com/ingrejhon/blog-with-login/-/tree/back_laravel), [front_angular](https://gitlab.com/ingrejhon/blog-with-login/-/tree/front-angular)

### Instalación Backend
1. Clonar la rama [back_laravel](https://gitlab.com/ingrejhon/blog-with-login/-/tree/back_laravel)
2. Entrar a la carpeta ***blog-with-login*** y posterior ejecutar **composer install**, **php artisan key: generate**.
3. Ejecutar **php artisan migrate** para crear las tablas en la base de datos, antes de esto debe crear la cadena de conexión en la raíz del proyecto en el archivo **.env**, ejemplo:
```env
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:J0Sk/XcPH5oCgZVqZXcYiG/yufGhRtV7pdtXfhScdgg=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=pgsql
DB_HOST=127.0.0.1
DB_PORT=5432
DB_DATABASE=jhon
DB_USERNAME=jhon
DB_PASSWORD=alexander1995

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DRIVER=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

```

3. Una vez realizado todo lo anterior si esta en linux ejecutar el comando ***bash run.sh***, por el contrario si esta en windows ejecutar ***php artisan serve***

### Instalación Frontend
1. Clonar la rama [front_angular](https://gitlab.com/ingrejhon/blog-with-login/-/tree/front-angular).
2. Entrar a la carpeta ***blog-with-login*** y posterior ejecutar **npm install**.
3. Una vez realizado todo lo anterior si esta en linux ejecutar el comando ***bash runAngular.sh***, por el contrario si esta en windows ejecutar ***ng serve***
4. Una vez hecho esto podemos proceder a ver en el navegador [http://localhost:4200](http://localhost:4200)

### Endpoints API
Para usar los endpoints de la api puede realizarlos y/o comprobarlos de las siguientes 3 formas distintas:

1. Puede usar Postman, Insomnia (Este es el que yo uso) para importar directamente la **Collection** que se encuentra en ***back_laravel***, en la raíz llamada ***Insomnia_2021-11-18.json***
2. Usar el portal ya montado en la instancia de AWS Amazon, [http://35.87.0.239:4200/#/](http://35.87.0.239:4200/#/)
3. Puede usar los endpoints que se encuentran descritos a continuación digitandolos en su cliente API de preferencia:
    <br>
    - Login Usuario:(POST) http://35.87.0.239:8000/api/login/
     json: 
    {
        "email": "ingrejhon@gmail.com",
        "password": "alexander1995"
    }
    <br>
    - Registro de usuario:(POST) http://35.87.0.239:8000/api/register/
     json:
    {
        "email": "juanito@gmail.com",
        "password": "juanito123",
        "password_confirmation": "juanito123",
        "name": "juanito prueba"
    }
    <br>
    - Todos los post: (GET) http://35.87.0.239:8000/api/posts/
    <br>
    **Los métodos nombrados a continuación requieren autenticación con Bearer Token (JWT) de lo contrario no podrán realizar el registro.**
    <br>
    - Crear un Post: (POST) http://35.87.0.239:8000/api/posts/
    json:
    {
        "title": "asdf",
        "description": "pp",
        "user_id": 1
    }
    <br>
    - Obtener un Post por user_id: (GET) http://35.87.0.239:8000/api/posts/1
