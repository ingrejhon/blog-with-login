<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function index()
    {
        return Post::with('user_id')->get();
    }

    public function post(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|string',
            'description' => 'required|string',
            'user_id' => 'required|integer',
        ]);

        $regex = "/^$/";

        if (preg_match($regex, $request->get('title'))){
            return response()->json(['message' => 'Empty Title'], 400);
        }else if (preg_match($regex, $request->get('description'))){
            return response()->json(['message' => 'Empty Description'], 400);
        }else if (preg_match($regex, $request->get('user_id'))){
            return response()->json(['message' => 'Empty user_id'], 400);
        }

        $new_post = Post::create([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'user_id' => $request->get('user_id')
        ]);

        return response()->json(['post' => $new_post], 201);
    }

    public function get_by_user_id($user_id){
        $regex = "/^$/";

        if (preg_match($regex, $user_id)){
            return response()->json(['message' => 'Empty user_id'], 400);
        }

        $get_posts = Post::with('user_id')->where('user_id', $user_id)->get();
        return response()->json(['post' => $get_posts], 200);
    }
}
